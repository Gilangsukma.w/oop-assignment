const ThreeDimention = require("./ThreeDimension");

class Beam extends ThreeDimention {
  constructor(length, width, height) {
    super("Beam");
    this.length = length;
    this.width = width;
    this.height = height;
  }

  // Overloading
  calculateVolume(name) {
    super.calculateVolume();
    const length = 10;
    const width = 5;
    const height = 2;
    const result = length * width * height;
    console.log(`${name} is trying to calculate Volume: ${result} cm`);
    return result;
  }

  calculateArea(name) {
    super.calculateArea();
    const length = 10;
    const width = 5;
    const height = 2;
    const result = 2 * (length * width + width * height + length * height);
    console.log(`${name} is trying to calculate Area: ${result} cm2`);
    return result;
  }
}

module.exports = Beam;
