const Geometry = require("../Geometry");

class ThreeDimension extends Geometry {
  constructor(name) {
    super(name, "Three Dimension");

    // Abstract
    if (this.constructor === ThreeDimension) {
      throw new Error("Three Diemension is abstract");
    }
  }

  calculateVolume() {
    return console.log("====== Calculating Volume ======");
  }

  calculateArea() {
    return console.log("====== Calculating Area ======");
  }
}

module.exports = ThreeDimension;
