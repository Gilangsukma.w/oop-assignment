const ThreeDimension = require("./ThreeDimension");

class Cone extends ThreeDimension {
  constructor(height, radius, leteral_surface) {
    super("Cone");
    this.height = height;
    this.radius = radius;
    this.leteral_surface = leteral_surface;
  }

  calculateVolume(name) {
    super.calculateVolume();
    //1/3 x π x r x r x t
    const formula = (1 / 3) * Math.PI * this.radius * this.radius * this.height;
    return `${name}, ${formula}`;
  }

  calculateArea(name) {
    super.calculateArea();
    const circleArea = Math.PI * this.radius * this.radius;
    const formula = circleArea + this.leteral_surface;
    return `${name}, ${formula} `;
  }
}

module.exports = Cone;
