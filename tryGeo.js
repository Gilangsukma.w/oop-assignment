// Freddy
const Square = require("./2D/square");
const Rectangle = require("./2D/rectangle");
const Triangle = require("./2D/triangle");

// Gilang
const Cone = require("./3D/Cone");
const Tube = require("./3D/Tube");

// Rangga
const Beam = require("./3D/Beam");
const Cube = require("./3D/Cube");

// Freddy
console.log("==== = = = = = === = = = ===== ======\n");
console.log("==== ==T=W=O== D=I M=E=N T=I O=N= ===\n");
console.log("==== = = = = = === = = = == = = = = =\n");
let trySquare = new Square(17);
console.log(trySquare);
trySquare.calculateArea();
trySquare.calculateCircumference();

let tryRectangle = new Rectangle(11, 12);
console.log(tryRectangle);
tryRectangle.calculateArea();
tryRectangle.calculateCircumference();

let tryTriangle = new Triangle(55, 12);
console.log(tryTriangle);
tryTriangle.calculateArea();
tryTriangle.calculateCircumference();

console.log("=====================================\n");

//Gilang
const cone = new Cone(3, 2, 5);
const ConeVolume = cone.calculateVolume("Cone Volume");
console.log(`${ConeVolume}\n`);

const ConeArea = cone.calculateArea("Cone Area");
console.log(`${ConeArea}\n`);

const sumCone = ConeVolume + ConeArea;
console.log("Sum Cone : ", sumCone);

const tube = new Tube(7, 18);
const TubeVolume = tube.calculateVolume("Tube Volume");
console.log(`${TubeVolume}\n`);

const TubeArea = tube.calculateArea("Tube Area");
console.log(`${TubeArea}\n`);

const sumTube = TubeVolume + TubeArea;

console.log("Sum Tube : ", sumTube);

//Rangga
const Beam1 = new Beam();
const Beam1Volume = Beam1.calculateVolume("Rangga Adhi Setyawan");
const Beam1Area = Beam1.calculateArea("Rangga Adhi Setyawan");
const SumBeam = Beam1Volume + Beam1Area;

const Cube1 = new Cube(16);
const Cube1Volume = Cube1.calculateVolume("Rangga Adhi Setyawan");
const Cube1Area = Cube1.calculateArea("Rangga Adhi Setyawan");
const SumCube = Cube1Volume + Cube1Area;

console.log(`\nThe sum Cube is ${SumCube} and sum Beam is ${SumBeam}`);
